import angular from 'angular';

let headerComponent = {
  template: require('./header.html'),
  controllerAs: 'appHeader',
  controller: function($scope) {
    const vm = this;
  }
}

export default
	angular.module('main.header', [])
		.component('appHeader', headerComponent).name;
