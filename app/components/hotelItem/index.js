import angular from 'angular';

let hotelItemComponent = {
	template: require('./hotelItem.template.html'),
	controllerAs: 'hotelItem',
	bindings: {
		hotel: '<'
	},
	controller: function($scope) {
	}
}

export default
	angular.module('main.hotelItem', [])
		.component('hotelItem', hotelItemComponent).name;
