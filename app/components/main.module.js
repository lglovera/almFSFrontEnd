require('../../node_modules/bootstrap/dist/css/bootstrap.css');
require('../../node_modules/font-awesome/css/font-awesome.css');
require('../../node_modules/bootstrap/dist/js/bootstrap.js');
require('../../node_modules/jquery/dist/jquery.js');

import angular from 'angular';
import routing from './main.route';
import component from './main.component';
import service from './main.service';

import header from './header'
import filter from './filter'
import hotelList from './hotelList'


angular
  .module('main', [header,filter,hotelList])
  .component('main', component)
  .factory('mainService', service)
  .config(routing);
