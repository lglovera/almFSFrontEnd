import angular from 'angular';
import hotelItem from '../hotelItem'
var _ = require('lodash');

let hotelListComponent = {
	template: require('./hotelList.template.html'),
	controllerAs: 'hotelList',
	bindings: {
		params: '=',
		hotels: '<',
	},
	controller: function($scope) {
	/*	$scope.hotels = [{
			name : 'Hotel Emperador',
			stars : 3,
			price : 1596,
		},{
			name : 'Petit Palace San Bernardo',
			stars : 4,
			price : 2145,
		},{
			name : 'Hotel Nuevo Boston',
			stars : 2,
			price : 861,
		}];*/

		$scope.filterFn = (hotel) => {
			let minPrice = false;
			let maxPrice = false;
			let name = false;
			let stars = false;

			if ($scope.hotelList.params) {
				if ($scope.hotelList.params.price[0] < hotel.price) {
					minPrice = true;
				}

				if ($scope.hotelList.params.price[1] > hotel.price) {
					maxPrice = true;
				}
			}

			if ($scope.hotelList.params.name) {
				name = hotel.name.includes($scope.hotelList.params.name);
			} else {
				name = true;
			}

			if ($scope.hotelList.params.stars.length !== 0) {
				stars = _.some($scope.hotelList.params.stars, (starsQty) => {
					if (!starsQty.quantity) {
						return true
					} else {
						return starsQty.quantity === hotel.stars;
					}
				});
			} else {
				stars = true;
			}

			return minPrice && maxPrice && name && stars;
		}
	}
}

export default
	angular.module('main.hotelList', [ hotelItem ])
		.component('hotelList', hotelListComponent).name;
