function mainRoutes($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.when('', '/main');
  $urlRouterProvider.when('/', '/main');

  $stateProvider
    .state('main', {
      url: '/main',
      component: 'main'
    })
}

export default mainRoutes;
