import mainHtml from './main.html';

let mainComponent = {
  template: mainHtml,
  controllerAs: 'main',
  controller: function($scope, mainService) {
    const vm = this;
    vm.title = mainService.title();

    $scope.filterParams = {
      name : undefined,
      price : {},
      stars : []
    }

    mainService.getHotels($scope);
  }
}

export default mainComponent;
