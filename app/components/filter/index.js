import angular from 'angular';
//import angularSlider from '../../../node_modules/angularjs-slider/dist/rzslider.js';
//require ('../../../node_modules/angularjs-slider/dist/rzslider.css');

require('../../../node_modules/bootstrap-slider/dist/bootstrap-slider.js');
require('../../../node_modules/bootstrap-slider/dist/css/bootstrap-slider.css');
require('../../../node_modules/angular-bootstrap-slider/slider.js');


let component = {
	template: require('./template.html'),
	controllerAs: 'filter',
	bindings: {
		params: '='
	},
	controller: function($scope) {

		$scope.stars = [
			{
				quantity: undefined,
			},{
				quantity: 5,
			},{
				quantity: 4,
			},{
				quantity: 3,
			},{
				quantity: 2,
			},{
				quantity: 1,
			}
		];

		$scope.selection = [];

		$scope.toggleSelection = function toggleSelection(star) {
			var idx = $scope.filter.params.stars.indexOf(star);

			if (idx > -1) {
				$scope.filter.params.stars.splice(idx, 1);
			} else {
				$scope.filter.params.stars.push(star);
			}
		};
	}
}

export default
	angular.module('main.filter', [ 'ui.bootstrap-slider' ])
		.component('filter', component).name;

