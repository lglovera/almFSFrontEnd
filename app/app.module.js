import angular from 'angular';
import uirouter from 'angular-ui-router';
import main from './components/main.module';

require('./main.scss');

angular.module('app', [
  uirouter,
  'main'
]);
