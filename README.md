##### Quick start

```
# clone
# --depth 1 removes all but one .git commit history
git clone https://gitlab.com/lglovera/almFSFrontEnd.git

# change directory to repo
cd almFSFrontEnd

# install the repo with npm
npm install

# start the server
npm start

```

go to [http://0.0.0.0:9000](http://0.0.0.0:9000) or [http://localhost:9000](http://localhost:9000) in your browser

